import { createApp } from 'vue'
import Footer from './Footer.vue'
import './assets/tailwind.css'
import './assets/main.scss'
createApp(Footer).mount('#app')
